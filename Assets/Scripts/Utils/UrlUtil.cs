﻿namespace ShowReel
{
    public static class UrlUtil
    {
        public static string Combine(string part1, string part2)
        {
            return part1 + "/" + part2;
        }
    }
}