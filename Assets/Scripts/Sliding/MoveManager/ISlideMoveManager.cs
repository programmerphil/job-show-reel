﻿namespace ShowReel
{
    public interface ISlideMoveManager
    {
        bool CanMoveSlidesRight();

        void MoveSlidesRight();

        bool CanMoveSlidesLeft();

        void MoveSlidesLeft();
    }
}