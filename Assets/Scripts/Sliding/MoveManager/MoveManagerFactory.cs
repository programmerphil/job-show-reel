﻿using System;
using System.Collections.Generic;

namespace ShowReel
{
    public class SlideMoveManagerFactory
    {
        public ISlideMoveManager CreateSlideMoveManager(SlideMoveType slideMoveType, IEnumerable<Slide> slides, SlidePointManager slidePointManager)
        {
            switch (slideMoveType)
            {
                case SlideMoveType.Finite:
                    return new FiniteSlideMoveManager(slides, slidePointManager);

                case SlideMoveType.Inifinte:
                    return new InfiniteSlideMoveManager(slides);

                default:
                    throw new ArgumentOutOfRangeException(nameof(slideMoveType));
            }
        }
    }
}