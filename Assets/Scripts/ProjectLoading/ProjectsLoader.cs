﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace ShowReel
{
    public class ProjectsLoader
    {
        public IEnumerable<ProjectData> LoadProjects()
        {
            ProjectsMetadata projectsMetadata = LoadProjectsMetaData();
            List<ProjectData> projects = new List<ProjectData>();

            foreach (var projectPath in projectsMetadata.ProjectDataPaths)
            {
                string projectDataJson = ReadTextFileFromFilePath(projectPath);

                projects.Add(JsonUtility.FromJson<ProjectData>(projectDataJson));
            }

            return projects;
        }

        private ProjectsMetadata LoadProjectsMetaData()
        {
            string projectsMetaDataPath = Path.Combine(Application.streamingAssetsPath, ApplicationConfig.GetProjectsMetaDataPath());

            string projectsMetaDataJson = ReadTextFileFromFilePath(projectsMetaDataPath);

            return JsonUtility.FromJson<ProjectsMetadata>(projectsMetaDataJson);
        }

        private string ReadTextFileFromFilePath(string path)
        {
            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }

            throw new System.Exception("File at path : " + path + " does not exist");
        }
    }
}