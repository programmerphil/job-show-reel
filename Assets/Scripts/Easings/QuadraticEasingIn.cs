﻿namespace ShowReel
{
    public class QuadraticEasingIn : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            return changeValue / 2 * t * t + startValue;
        }
    }
}