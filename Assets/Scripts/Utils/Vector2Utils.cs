﻿using UnityEngine;

namespace ShowReel.Utils
{
    public static class Vector2Utils
    {
        public static Vector3 AsVector3(this Vector2 vector2)
        {
            return new Vector3(vector2.x, vector2.y);
        }
    }
}