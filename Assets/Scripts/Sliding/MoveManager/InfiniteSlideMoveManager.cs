﻿using System.Collections.Generic;

namespace ShowReel
{
    public class InfiniteSlideMoveManager : BaseSlideMoveManager
    {
        public InfiniteSlideMoveManager(IEnumerable<Slide> slides) : base(slides)
        {
        }

        public override bool CanMoveSlidesLeft()
        {
            return true;
        }

        public override bool CanMoveSlidesRight()
        {
            return true;
        }
    }
}