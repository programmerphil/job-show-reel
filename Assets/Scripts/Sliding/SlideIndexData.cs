﻿namespace ShowReel
{
    public class SlideIndexData
    {
        public int Index { get; }
        public bool HasIndexBeenLoopedToOppositeSide { get; }

        public SlideIndexData(int index, bool hasIndexBeenLoopedToOppositeSide)
        {
            Index = index;
            HasIndexBeenLoopedToOppositeSide = hasIndexBeenLoopedToOppositeSide;
        }
    }
}