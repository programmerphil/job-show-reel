﻿namespace ShowReel
{
    public static class Easings
    {
        public static Easing LinearInOut { get { return new Easing(new LinearEasing()); } }

        public static Easing QuadraticInOut { get { return new Easing(new QuadraticEasingIn(), new QuadraticEasingOut()); } }

        public static Easing CubicInOut { get { return new Easing(new CubicEasingIn(), new CubicEasingOut()); } }

        public static Easing QuarticInOut { get { return new Easing(new QuarticEasingIn(), new QuarticEasingOut()); } }
    }
}