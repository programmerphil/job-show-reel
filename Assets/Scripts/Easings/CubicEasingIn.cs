﻿namespace ShowReel
{
    public class CubicEasingIn : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            return changeValue / 2 * t * t * t + startValue;
        }
    }
}