﻿using System.Collections.Generic;

namespace ShowReel
{
    public abstract class BaseSlideMoveManager : ISlideMoveManager
    {
        protected IEnumerable<Slide> _slides;

        public BaseSlideMoveManager(IEnumerable<Slide> slides)
        {
            _slides = slides;
        }

        public void MoveSlidesLeft()
        {
            if (CanMoveSlidesLeft())
            {
                foreach (var slide in _slides)
                {
                    slide.MoveLeft();
                }
            }
        }

        public void MoveSlidesRight()
        {
            if (CanMoveSlidesRight())
            {
                foreach (var slide in _slides)
                {
                    slide.MoveRight();
                }
            }
        }

        public abstract bool CanMoveSlidesLeft();

        public abstract bool CanMoveSlidesRight();
    }
}