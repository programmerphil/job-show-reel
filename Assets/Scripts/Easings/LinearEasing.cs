﻿namespace ShowReel
{
    public class LinearEasing : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            return changeValue * t + startValue;
        }
    }
}