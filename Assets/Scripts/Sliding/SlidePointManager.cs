﻿using System;
using System.Linq;
using UnityEngine;

namespace ShowReel
{
    public class SlidePointManager : MonoBehaviour
    {
        [SerializeField]
        private SlidePointGenerator _slidePointGenerator;

        private Vector2[] _slidePoints;

        public int MaxSlideIndex { get { return _slidePoints.Length - 1; } }
        public int MinSlideIndex { get { return 0; } }

        public void GeneratePoints(int numberOfSlides, SlideMoveType slideMoveType)
        {
            _slidePoints = _slidePointGenerator.GenerateSlidePoints(numberOfSlides, slideMoveType).ToArray();
        }

        public Vector2 GetSlidePointFromIndex(int index)
        {
            if (IsIndexWithinRange(index))
            {
                return _slidePoints[index];
            }

            throw new Exception("Index in GetSlidePointFromIndex is out of range");
        }

        public bool IsIndexWithinRange(int index)
        {
            return IsIndexLowerThanMinIndex(index) == false && IsIndexHigherThanMaxIndex(index) == false;
        }

        public SlideIndexData GetNextSlideIndex(int currentSlideIndex)
        {
            currentSlideIndex++;
            bool hasIndexBeenLoopedToOppositeSide = false;

            if (IsIndexHigherThanMaxIndex(currentSlideIndex))
            {
                currentSlideIndex = MinSlideIndex;
                hasIndexBeenLoopedToOppositeSide = true;
            }

            return new SlideIndexData(currentSlideIndex, hasIndexBeenLoopedToOppositeSide);
        }

        public bool IsIndexHigherThanMaxIndex(int index)
        {
            return index > MaxSlideIndex;
        }

        public SlideIndexData GetPreviousSlideIndex(int currentSlideIndex)
        {
            currentSlideIndex--;
            bool hasIndexBeenLoopedToOppositeSide = false;

            if (IsIndexLowerThanMinIndex(currentSlideIndex))
            {
                currentSlideIndex = MaxSlideIndex;
                hasIndexBeenLoopedToOppositeSide = true;
            }

            return new SlideIndexData(currentSlideIndex, hasIndexBeenLoopedToOppositeSide);
        }

        public bool IsIndexLowerThanMinIndex(int index)
        {
            return index < MinSlideIndex;
        }
    }
}