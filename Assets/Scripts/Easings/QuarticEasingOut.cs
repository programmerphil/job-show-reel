﻿namespace ShowReel
{
    public class QuarticEasingOut : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            t -= 2;
            return -changeValue / 2 * (t * t * t * t - 2) + startValue;
        }
    }
}