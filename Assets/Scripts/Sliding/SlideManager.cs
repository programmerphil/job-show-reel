﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ShowReel
{
    public class SlideManager : MonoBehaviour
    {
        [SerializeField]
        private SlidePointManager _slidePointManager;

        [SerializeField]
        private Transform _slideRootParent;

        [SerializeField]
        private Slide _slidePrefab;

        [SerializeField]
        private UnityEngine.UI.Button _leftArrow;

        [SerializeField]
        private UnityEngine.UI.Button _rightArrow;

        private IEnumerable<SlideData> _slidedataEntries;
        private List<Slide> _slides;
        private ProjectsLoader _projectsLoader;
        private ISlideMoveManager _slideMoveManager;
        private SlideMoveManagerFactory _slideMoveManagerFactory;

        private void Awake()
        {
            _slides = new List<Slide>();
            _projectsLoader = new ProjectsLoader();
            _slideMoveManagerFactory = new SlideMoveManagerFactory();
        }

        private void Start()
        {
            _slidedataEntries = CreateSlideDataEntriesFromProjectData();
            SlideMoveType slideMoveType = GetSlideMoveType(_slidedataEntries.Count());
            _slidePointManager.GeneratePoints(_slidedataEntries.Count(), slideMoveType);
            SpawnSlides();
            _slideMoveManager = _slideMoveManagerFactory.CreateSlideMoveManager(slideMoveType, _slides, _slidePointManager);
            UpdateArrowButtons();
        }

        private SlideMoveType GetSlideMoveType(int numberOfSlides)
        {
            if (numberOfSlides < 3)
            {
                return SlideMoveType.Finite;
            }
            else
            {
                return SlideMoveType.Inifinte;
            }
        }

        private IEnumerable<SlideData> CreateSlideDataEntriesFromProjectData()
        {
            foreach (var projectData in _projectsLoader.LoadProjects())
            {
                yield return new SlideData(projectData.ProjectId, Resources.Load<Sprite>(projectData.PathToProjectSprite));
            }
        }

        private void SpawnSlides()
        {
            int i = 0;
            foreach (var slideDataEntry in _slidedataEntries)
            {
                Vector2 spawnPosition = _slidePointManager.GetSlidePointFromIndex(i);
                Slide spawnedSlide = Instantiate(_slidePrefab);
                Slide slide = Slide.Create(spawnedSlide, i, slideDataEntry, spawnPosition, _slideRootParent, _slidePointManager);
                _slides.Add(slide);
                i++;
            }
        }

        public void MoveRight()
        {
            _slideMoveManager.MoveSlidesRight();
            UpdateArrowButtons();
        }

        public void MoveLeft()
        {
            _slideMoveManager.MoveSlidesLeft();
            UpdateArrowButtons();
        }

        private void UpdateArrowButtons()
        {
            _leftArrow.interactable = _slideMoveManager.CanMoveSlidesLeft();
            _rightArrow.interactable = _slideMoveManager.CanMoveSlidesRight();
        }
    }
}