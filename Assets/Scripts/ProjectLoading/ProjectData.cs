﻿using System;

namespace ShowReel
{
    [Serializable]
    public class ProjectData
    {
        public string ProjectId;
        public string PathToMainScene;
        public string PathToProjectSprite;

        public ProjectData()
        {
            ProjectId = Guid.NewGuid().ToString();
        }
    }
}