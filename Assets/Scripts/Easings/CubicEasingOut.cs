﻿namespace ShowReel
{
    public class CubicEasingOut : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            t -= 2;
            return changeValue / 2 * (t * t * t + 2) + startValue;
        }
    }
}