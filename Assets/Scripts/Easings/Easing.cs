﻿using System;
using UnityEngine;

namespace ShowReel
{
    public class Easing
    {
        private BaseEasing _easingIn;
        private BaseEasing _easingOut;

        public Easing(BaseEasing easing)
        {
            _easingIn = easing;
            _easingOut = easing;
        }

        public Easing(BaseEasing easingIn, BaseEasing easingOut)
        {
            _easingIn = easingIn;
            _easingOut = easingOut;
        }

        public Vector2 GetPointFromTime(Vector2 startPoint, Vector2 endPoint, float time, float duration)
        {
            float t = time / (duration / 2);

            if (t <= 1 && t >= 0)
            {
                return _easingIn.GetPointOnCurveFromTime(startPoint, endPoint, t);
            }
            else if (t >= 1 && t <= 2)
            {
                return _easingOut.GetPointOnCurveFromTime(startPoint, endPoint, t);
            }

            throw new Exception("T is not within acceptable range. T is " + t);
        }
    }
}