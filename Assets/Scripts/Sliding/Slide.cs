﻿using System;
using UnityEngine;

namespace ShowReel
{
    public class Slide : MonoBehaviour
    {
        [SerializeField]
        private UnityEngine.UI.Button _slideButton;

        [SerializeField]
        private SlideMover _pointMover2D;

        [SerializeField]
        private RectTransform _rectTransform;

        private SlidePointManager _slidePointManager;

        public int CurrentSlideIndex { get; private set; }

        public string SlideId { get; private set; }

        public event Action SlideClicked;

        public static Slide Create(Slide slide, int startIndex, SlideData slideData, Vector2 spawnPosition, Transform parent, SlidePointManager slidePointManager)
        {
            slide.transform.position = spawnPosition;
            slide.transform.SetParent(parent, false);

            slide.SlideId = slideData.SlideId;
            slide.CurrentSlideIndex = startIndex;
            slide.SetSlideSprite(slideData.SlideSprite);
            slide._slidePointManager = slidePointManager;

            return slide;
        }

        private void SetSlideSprite(Sprite slideSprite)
        {
            _slideButton.image.sprite = slideSprite;
        }

        private void Start()
        {
            if (_slidePointManager == null)
            {
                throw new NullReferenceException(nameof(_slidePointManager) + " cant be null");
            }

            _slideButton.onClick.AddListener(() => SlideClicked?.Invoke());
        }

        private void OnDisable()
        {
            _slideButton.onClick.RemoveAllListeners();
        }

        public void MoveRight()
        {
            SlideIndexData slideIndexData = _slidePointManager.GetNextSlideIndex(CurrentSlideIndex);
            CurrentSlideIndex = slideIndexData.Index;

            SetNewTarget(slideIndexData.HasIndexBeenLoopedToOppositeSide);
        }

        public void MoveLeft()
        {
            SlideIndexData slideIndexData = _slidePointManager.GetPreviousSlideIndex(CurrentSlideIndex);
            CurrentSlideIndex = slideIndexData.Index;

            SetNewTarget(slideIndexData.HasIndexBeenLoopedToOppositeSide);
        }

        private void SetNewTarget(bool hasIndexBeenLoopedToOppositeSide)
        {
            Vector2 newTarget = _slidePointManager.GetSlidePointFromIndex(CurrentSlideIndex);

            if (hasIndexBeenLoopedToOppositeSide)
            {
                _rectTransform.anchoredPosition = newTarget;
            }
            else
            {
                _pointMover2D.SetTarget(newTarget, Easings.QuarticInOut);
            }
        }
    }
}