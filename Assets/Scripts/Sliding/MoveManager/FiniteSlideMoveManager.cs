﻿using System.Collections.Generic;

namespace ShowReel
{
    public class FiniteSlideMoveManager : BaseSlideMoveManager
    {
        private SlidePointManager _slidePointManager;

        public FiniteSlideMoveManager(IEnumerable<Slide> slides, SlidePointManager slidePointManager) : base(slides)
        {
            _slidePointManager = slidePointManager;
        }

        public override bool CanMoveSlidesLeft()
        {
            foreach (var slide in _slides)
            {
                int nextSlideIndex = slide.CurrentSlideIndex - 1;
                if (_slidePointManager.IsIndexLowerThanMinIndex(nextSlideIndex))
                {
                    return false;
                }
            }

            return true;
        }

        public override bool CanMoveSlidesRight()
        {
            foreach (var slide in _slides)
            {
                int nextSlideIndex = slide.CurrentSlideIndex + 1;
                if (_slidePointManager.IsIndexHigherThanMaxIndex(nextSlideIndex))
                {
                    return false;
                }
            }
            return true;
        }
    }
}