﻿using UnityEngine;

namespace ShowReel
{
    public class ApplicationConfig
    {
        private const string _projectsMetaDataRelativePath = "Projects/ProjectsMetaData.json";
        private const string _projectsFolderName = "Projects";

        public static string GetProjectsMetaDataPath()
        {
            return UrlUtil.Combine(Application.streamingAssetsPath, _projectsMetaDataRelativePath);
        }

        public static string GetProjectsFolderDirectory()
        {
            return UrlUtil.Combine(Application.streamingAssetsPath, _projectsFolderName);
        }

        public static string GetResourceProjectsFolderDirectory()
        {
            return _projectsFolderName;
        }
    }
}