﻿using UnityEngine;

namespace ShowReel
{
    public class SlideMover : MonoBehaviour
    {
        [SerializeField]
        private float _duration;

        [SerializeField]
        private RectTransform _rectTransform;

        private Vector2 _target;
        private bool _canMove;
        private float _currentTime;
        private Easing _easing;
        private Vector2 _startPosition;

        public void SetTarget(Vector2 target, Easing easing)
        {
            _target = target;
            _startPosition = _rectTransform.anchoredPosition;
            _easing = easing;
            _currentTime = 0;
            _canMove = true;
        }

        private void Update()
        {
            if (HasDurationEnded())
            {
                _canMove = false;
            }
            if (_canMove)
            {
                _rectTransform.anchoredPosition += GetMoveVector();
                _currentTime += Time.deltaTime;
            }
        }

        private Vector2 GetMoveVector()
        {
            return GetNextPointOnCurve() - _rectTransform.anchoredPosition;
        }

        private Vector2 GetNextPointOnCurve()
        {
            return _easing.GetPointFromTime(_startPosition, _target, _currentTime, _duration);
        }

        private bool HasDurationEnded()
        {
            return _currentTime >= _duration;
        }
    }
}