﻿using System;
using UnityEngine;

namespace ShowReel
{
    [Serializable]
    public class ProjectsMetadata
    {
        [SerializeField]
        public string[] ProjectDataPaths;
    }
}