﻿using UnityEngine;

namespace ShowReel
{
    public abstract class BaseEasing
    {
        public Vector2 GetPointOnCurveFromTime(Vector2 startPoint, Vector2 endPoint, float t)
        {
            float pointOnCurveX = GetPointFromTime(t, startPoint.x, endPoint.x - startPoint.x);
            float pointOnCurveY = GetPointFromTime(t, startPoint.y, endPoint.y - startPoint.y);

            return new Vector2(pointOnCurveX, pointOnCurveY);
        }

        protected abstract float GetPointFromTime(float t, float startValue, float changeValue);
    }
}