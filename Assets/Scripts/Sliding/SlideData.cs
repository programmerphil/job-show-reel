﻿using UnityEngine;

namespace ShowReel
{
    public class SlideData
    {
        public string SlideId { get; }
        public Sprite SlideSprite { get; }

        public SlideData(string slideId, Sprite slideSprite)
        {
            SlideId = slideId;
            SlideSprite = slideSprite;
        }
    }
}