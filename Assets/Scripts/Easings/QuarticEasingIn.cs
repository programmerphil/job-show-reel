﻿namespace ShowReel
{
    public class QuarticEasingIn : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            return changeValue / 2 * t * t * t * t + startValue;
        }
    }
}