﻿namespace ShowReel
{
    public class QuadraticEasingOut : BaseEasing
    {
        protected override float GetPointFromTime(float t, float startValue, float changeValue)
        {
            t--;
            return -(changeValue / 2) * (t * (t - 2) - 1) + startValue;
        }
    }
}