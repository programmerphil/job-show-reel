﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ShowReel
{
    public class SlidePointGenerator : MonoBehaviour
    {
        [SerializeField]
        private Vector2 _distanceBetweenEachSlide;

        [SerializeField]
        private Vector2 _centerPoint;

        public IEnumerable<Vector2> GenerateSlidePoints(int numberOfSlides, SlideMoveType slideMoveType)
        {
            Vector2 startPoint = CalculateStartPoint(numberOfSlides);
            int numberOfPointsToGenerate = CalculateNumberOfPointsToGenerate(numberOfSlides, slideMoveType);

            for (int i = 0; i < numberOfPointsToGenerate; i++)
            {
                yield return startPoint + (i * _distanceBetweenEachSlide);
            }
        }

        private int CalculateNumberOfPointsToGenerate(int numberOfSlides, SlideMoveType slideMoveType)
        {
            switch (slideMoveType)
            {
                case SlideMoveType.Inifinte:
                    return numberOfSlides;

                case SlideMoveType.Finite:
                    return numberOfSlides + CalculateNumberOfSlidesLeftFromCenter(numberOfSlides);

                default:
                    throw new ArgumentOutOfRangeException(nameof(slideMoveType));
            }
        }

        private Vector2 CalculateStartPoint(int numberOfSlides)
        {
            int numberOfSlidesLeftOfCenter = CalculateNumberOfSlidesLeftFromCenter(numberOfSlides);

            return _centerPoint - (numberOfSlidesLeftOfCenter * _distanceBetweenEachSlide);
        }

        private int CalculateNumberOfSlidesLeftFromCenter(int numberOfSlides)
        {
            return Mathf.FloorToInt(numberOfSlides / 2);
        }
    }
}